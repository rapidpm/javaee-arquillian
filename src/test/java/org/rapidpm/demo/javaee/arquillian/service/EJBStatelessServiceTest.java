package org.rapidpm.demo.javaee.arquillian.service;

import java.io.File;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.exporter.ZipExporter;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.descriptor.api.Descriptors;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;

/**
 * EJBStatelessService Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Mai 29, 2013</pre>
 */
@RunWith(Arquillian.class)
public class EJBStatelessServiceTest {

//    @Deployment(name = "datasource", order = 1 , managed = true)
//    public static Archive<?> createDeploymentdataSource() {
//        final JavaArchive deployment = ShrinkWrap.create(JavaArchive.class,"datasource.jar");
//        final File[] libs = Maven.resolver()
//                .loadPomFromFile("pom.xml").resolve("com.oracle:ojdbc6","com.oracle:orai18n").withoutTransitivity()
//                .asFile();
//
//        final JavaArchive imported = ShrinkWrap.create(ZipImporter.class,"driver.jar").importFrom(libs[0]).importFrom(libs[1]).as(JavaArchive.class);
//
//        deployment.merge(imported);
//        return deployment;
//    }
//
//    @Deployment(name = "datasourceConfig", order = 2 , managed = true)
//    public static Archive<?> createDeploymentdataSourceConfig() {
//        final JavaArchive javaArchive = ShrinkWrap.create(JavaArchive.class).addAsResource("META-INF/jbossas-ds.xml");
//        return javaArchive;
//    }
//





//    @Deployment(name = "testDeployment",order = 3)
    @Deployment(name = "testDeploymentJar")
    public static Archive<?> createDeploymentAsJar() {
        final Class<EJBStatelessService> mainClass2Test = EJBStatelessService.class;
        final String archiveName = mainClass2Test.getSimpleName() + ".jar";

        final Archive<?> javaArchive = ShrinkWrap.create(JavaArchive.class, archiveName)
                .addClasses(mainClass2Test, StatelessService.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
//        schreibe archiv auf Platte um es später zu verwenden
        javaArchive.as(ZipExporter.class).exportTo(new File("target/" + archiveName), true);
        return javaArchive;
    }

//    @Deployment(name = "testDeploymentWar")
//    public static Archive<?> createDeploymentAsWar() {
//        final Class<EJBStatelessService> mainClass2Test = EJBStatelessService.class;
//        final String archiveName = mainClass2Test.getSimpleName() + ".war";
//
//        final Archive<?> javaArchive = ShrinkWrap.create(WebArchive.class, archiveName)
//                .addClasses(mainClass2Test, StatelessService.class)
//                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
//
////        schreibe archiv auf Platte um es später zu verwenden
//        javaArchive.as(ZipExporter.class).exportTo(new File("target/" + archiveName), true);
//        return javaArchive;
//    }

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }


    @EJB
    private EJBStatelessService ejbStatelessService;

    /**
     * Method: firstService()
     */
    @Test @OperateOnDeployment("testDeploymentJar")
    public void testFirstServiceJar() throws Exception {
        Assert.assertNotNull(ejbStatelessService);
        final String s = ejbStatelessService.firstService();
        Assert.assertNotNull(s);
        Assert.assertFalse(s.isEmpty());
        System.out.println("s = " + s);

    }

//    @Test @OperateOnDeployment("testDeploymentWar")
//    public void testFirstServiceWar() throws Exception {
//        Assert.assertNotNull(ejbStatelessService);
//        final String s = ejbStatelessService.firstService();
//        Assert.assertNotNull(s);
//        Assert.assertFalse(s.isEmpty());
//        System.out.println("s = " + s);
//
//    }


} 
