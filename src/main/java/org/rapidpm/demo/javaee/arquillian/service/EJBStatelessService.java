package org.rapidpm.demo.javaee.arquillian.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * User: Sven Ruppert
 * Date: 29.05.13
 * Time: 10:56
 */
@Stateless
public class EJBStatelessService {

    @Inject
    private StatelessService statelessService;

    public String firstService() {
        return statelessService.firstService();
    }


}
