package org.rapidpm.demo.javaee.arquillian.service;

import javax.inject.Inject;

import org.junit.Assert;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;

/**
 * StatelessService Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Mai 29, 2013</pre>
 */
@RunWith(Arquillian.class)
public class StatelessServiceTest {

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(StatelessService.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }




    @Inject private StatelessService statelessService;

    @Test
    public void testFirstService001() throws Exception {
        Assert.assertNotNull(statelessService);
        final String s = statelessService.firstService();
        Assert.assertNotNull(s);
        Assert.assertFalse(s.isEmpty());
        System.out.println("s = " + s);

    }
}
